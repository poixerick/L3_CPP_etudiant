#include "Controleur.hpp"

#include <iostream>
#include <fstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    actualiser();
}

std::string Controleur::getTexte() {
    std::ostringstream ostringstream;
    _inventaire.trier();
    ostringstream << _inventaire;
    return ostringstream.str();
}

void Controleur::actualiser(){
    for (auto & v : _vues){
        v->actualiser();
    }
}

void Controleur::chargerInventaire(const std::string fileName){
    std::ifstream myfile(fileName);
    myfile >> _inventaire;
    actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}


