#include "Controleur.hpp"

int main(int argc, char ** argv) {
	Controleur viewer(argc, argv);
    VueConsole vue(viewer);
    viewer.run();
	return 0;
}

