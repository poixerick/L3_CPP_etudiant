#include "Inventaire.hpp"

std::ostream &operator<<(std::ostream &os, const Inventaire & i) {
    for(const auto bouteille : i._bouteilles){
        os << bouteille;
    }
    return os;
}

std::istream &operator>>(std::istream &is, Inventaire & i){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    std::string buffer;

    Bouteille b;
    while (std::getline(is, buffer, '\n')){
        std::istringstream istr(buffer);
        istr >> b;
        i._bouteilles.push_back(b);
    }
    std::locale::global(vieuxLoc);

    return is;
}

void Inventaire::trier() {
    _bouteilles.sort();
}
