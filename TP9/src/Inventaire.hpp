#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <sstream>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::list<Bouteille> _bouteilles;
    friend std::ostream & operator<<(std::ostream & os, const Inventaire & i);
    friend std::istream & operator>>(std::istream &is, Inventaire & i);
    void trier();
};

#endif
