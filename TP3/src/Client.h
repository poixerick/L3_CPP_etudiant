#ifndef L3_CPP_ETUDIANT_CLIENT_H
#define L3_CPP_ETUDIANT_CLIENT_H

#include <iostream>
class Client{
private:
    int _id;
    std::string _nom;
public:
    Client(int id, const std::string &nom);
    int getId() const;
    const std::string &getNom() const;
    void afficherClient() const;
};

#endif
