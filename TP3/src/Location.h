//
// Created by poixe on 23/03/2020.
//

#ifndef L3_CPP_ETUDIANT_LOCATION_H
#define L3_CPP_ETUDIANT_LOCATION_H

struct Location{
    int _idClient;
    int _idProduit;
    Location(int idCli, int idProd);
    void afficherLocation() const;
};
#endif //L3_CPP_ETUDIANT_LOCATION_H
