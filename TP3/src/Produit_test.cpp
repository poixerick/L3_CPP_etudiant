#include <CppUTest/CommandLineTestRunner.h>
#include "Produit.h"


TEST_GROUP(GroupProduit) {
};

TEST(GroupProduit, getters) {
    Produit produit = Produit(0, "produit");
    CHECK_EQUAL(produit.getId(), 0);
    CHECK_EQUAL(produit.getDescription(), "produit");
}
