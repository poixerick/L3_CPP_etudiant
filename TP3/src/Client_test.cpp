#include <CppUTest/CommandLineTestRunner.h>
#include "Client.h"


TEST_GROUP(GroupClient) {
};

TEST(GroupClient, getters) {
    Client client = Client(0, "Client");
    CHECK_EQUAL(client.getId(), 0);
    CHECK_EQUAL(client.getNom(), "Client");
}
