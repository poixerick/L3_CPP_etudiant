//
// Created by poixe on 23/03/2020.
//

#include "Location.h"
#include <iostream>

Location::Location(int idCli, int idProd){
    _idClient=idCli;
    _idProduit=idProd;
}

void Location::afficherLocation() const{
    std::cout << "Location (" << this->_idClient << ", " << this->_idProduit << ")" << std::endl;
}