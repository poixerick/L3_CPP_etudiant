//
// Created by erick on 23/03/2020.
//

#include "Magasin.h"
#include <iostream>

Magasin::Magasin() {
    _idCourantClient=0;
    _idCourantProduit=0;
}

int Magasin::nbClients() const{
    return _clients.size();
}

int Magasin::nbProduits() const{
    return _produits.size();
}

int Magasin::nbLocations() const{
    return _locations.size();
}

void Magasin::ajouterClient(const std::string &nom){
    _clients.push_back(Client(_idCourantClient,nom));
    _idCourantClient++;
}

void Magasin::ajouterProduit(const std::string &nom){
    _produits.push_back(Produit(_idCourantProduit,nom));
    _idCourantProduit++;
}

void Magasin::ajouterLocation(int idClient,int idProduit){
    bool add = true;
    for(const Location &_location : _locations){
        if(_location._idClient == idClient && _location._idProduit == idProduit){
            add=false;
        }
    }
    if(add){
        _locations.push_back(Location(idClient,idProduit));
    }
    else{
        throw std::string("ERREUR: cette location existe déjà");
    }
}

void Magasin::afficherClients() const{
    for(Client cli : _clients)
        cli.afficherClient();
}

void Magasin::afficherProduits() const{
    for(Produit pro : _produits)
        pro.afficherProduit();
}

void Magasin::afficherLocation() const{
    for(Location loc : _locations)
        loc.afficherLocation();
}

void Magasin::supprimerClient(int idClient){
    std::vector<Client> temp;
    for(const Client &_client : _clients){
        if(_client.getId() != idClient){
            temp.push_back(_client);
        }
    }
    if(temp.size() == nbClients()){
        throw std::string("ERREUR: ce client n'existe pas");
    }
    _clients.swap(temp);
}

void Magasin::supprimerProduit(int idProduit){
    std::vector<Produit> temp;
    for(const Produit &_produit : _produits){
        if(_produit.getId() != idProduit){
            temp.push_back(_produit);
        }
    }
    if(temp.size() == nbProduits()){
        throw std::string("ERREUR: ce produit n'existe pas");
    }
    _produits.swap(temp);
}

void Magasin::supprimerLocation(int idClient, int idProduit){
    bool del = false;
    std::vector<Location> temp;
    for(const Location &_location : _locations){
        if(_location._idClient == idClient && _location._idProduit == idProduit){
            del=true;
            temp.push_back(_location);
        }
    }
    if(!del){
        _locations.swap(temp);
    }
    else{
        throw std::string("ERREUR: cette location existe déjà");
    }
}

bool Magasin::trouverClientDansLocation(int idClient) const{
    for(const Location &_location : _locations){
        if(_location._idClient == idClient){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const{
    std::vector<int> temp;
    for(const Client &_client : _clients){
        if(not trouverClientDansLocation(_client.getId())){
            temp.push_back(_client.getId());
        }
    }
    return temp;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const{
    for(const Location &_location : _locations){
        if(_location._idProduit == idProduit){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const{
    std::vector<int> temp;
    for(const Produit &_produit : _produits){
        if(not trouverProduitDansLocation(_produit.getId())){
            temp.push_back(_produit.getId());
        }
    }
    return temp;
}