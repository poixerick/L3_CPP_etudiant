//
// Created by erick on 23/03/2020.
//

#ifndef L3_CPP_ETUDIANT_PRODUIT_H
#define L3_CPP_ETUDIANT_PRODUIT_H

#include <iostream>

class Produit{
private:
    int _id;
    std::string _description;
public:
    Produit(int id, const std::string &des);
    int getId() const;
    const std::string &getDescription() const;
    void afficherProduit() const;
};

#endif //L3_CPP_ETUDIANT_PRODUIT_H
