
#include <CppUTest/CommandLineTestRunner.h>
#include "Magasin.h"


TEST_GROUP(GroupMagasin) {
};


TEST(GroupMagasin, addClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Client");
    CHECK_EQUAL(magasin.nbClients(), 1);
}

TEST(GroupMagasin, delClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Client");
    magasin.supprimerClient(0);
    CHECK_EQUAL(magasin.nbClients(), 0);
}

TEST(GroupMagasin, errDelClient) {
    Magasin magasin = Magasin();
    magasin.ajouterClient("Client");
    CHECK_THROWS(std::string, magasin.supprimerClient(1));
}

TEST(GroupMagasin, addProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduit("produit");
    CHECK_EQUAL(magasin.nbProduits(), 1);
}

TEST(GroupMagasin, delProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduit("produit");
    magasin.supprimerProduit(0);
    CHECK_EQUAL(magasin.nbProduits(), 0);
}

TEST(GroupMagasin, errDelProduit) {
    Magasin magasin = Magasin();
    magasin.ajouterProduit("produit");
    CHECK_THROWS(std::string, magasin.supprimerProduit(1));
}
