#include "Magasin.h"

#include <iostream>

int main() {

    Location loc=Location(0,51);
    loc.afficherLocation();

    Client cli=Client(26, "client");
    cli.afficherClient();

    Produit pro=Produit(87, "produit");
    pro.afficherProduit();
    Magasin mag = Magasin();

    mag.ajouterClient("Client1");
    mag.ajouterClient("Client2");
    mag.afficherClients();

    mag.supprimerClient(0);
    mag.afficherClients();

    mag.ajouterProduit("Produit1");
    mag.ajouterProduit("Produit2");
    mag.afficherProduits();

    mag.supprimerProduit(0);
    mag.afficherProduits();

    return 0;
}


