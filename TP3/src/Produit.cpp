//
// Created by erick on 23/03/2020.
//

#include "Produit.h"
#include <iostream>

Produit::Produit(int id, const std::string &des) {
    _id=id;
    _description=des;
}

int Produit::getId() const{
    return _id;
}

const std::string &Produit::getDescription() const {
    return _description;
}

void Produit::afficherProduit() const{
    std::cout << "Produit (" << getId() << ", " << getDescription() << ")" << std::endl;
}