//
// Created by erick on 23/03/2020.
//

#ifndef L3_CPP_ETUDIANT_MAGASIN_H
#define L3_CPP_ETUDIANT_MAGASIN_H

#include <iostream>
#include <vector>
#include "Produit.h"
#include "Location.h"
#include "Client.h"

class Magasin{
private:
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;
public:
    Magasin();
    int nbClients() const;
    int nbProduits() const;
    int nbLocations() const;
    void ajouterClient(const std::string &nom);
    void ajouterProduit(const std::string &nom);
    void ajouterLocation(int idClient,int idProduit);
    void afficherClients() const;
    void afficherProduits() const;
    void afficherLocation() const;
    void supprimerClient(int idClient);
    void supprimerProduit(int idProduit);
    void supprimerLocation(int idClient, int idProduit);
    bool trouverClientDansLocation(int idClient) const;
    std::vector<int> calculerClientsLibres() const;
    bool trouverProduitDansLocation(int idProduit) const;
    std::vector<int> calculerProduitsLibres() const;
};

#endif //L3_CPP_ETUDIANT_MAGASIN_H
