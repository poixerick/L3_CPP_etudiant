#ifndef FIBONACCI
#define FIBONACCI

int fibonacciRecursif(int n);
int fibonacciIteratif(int n);

#endif