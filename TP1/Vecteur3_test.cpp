#include "Vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupeVect) {};

TEST(GroupeVect, testNorme){
  vecteur v= vecteur(1, 2, 3);
  float result = v.norme();
  float fl=3.74166;
  DOUBLES_EQUAL(fl, result, 1e-5);
} 

TEST(GroupeVect, testProd){ 
	vecteur v= vecteur(1, 2, 3); 
	vecteur v2= vecteur(2, 3, 4); 
	float result = v.produit(v2); 
	float fl=20; 
	DOUBLES_EQUAL(fl, result, 1e-5); 
}