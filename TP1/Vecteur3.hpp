#ifndef VECTEUR3
#define VECTEUR3

struct vecteur{
	float x,y,z;
	vecteur(float a, float b, float c);
	void afficher();
	float norme();
	float produit(vecteur vec2);
	vecteur somme(vecteur vec2);
};
#endif