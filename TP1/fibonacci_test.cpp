#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupeFibo) {};

TEST(GroupeFibo, testIter){
    int result=fibonacciIteratif(7);
    CHECK_EQUAL(13, result);
}

TEST(GroupeFibo, testRecur){
    int result=fibonacciRecursif(7);
    CHECK_EQUAL(13, result);
}