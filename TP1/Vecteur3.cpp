#include <iostream>
#include <cmath>
#include "Vecteur3.hpp"
using namespace std;


//Constructeur d'initialisation
vecteur::vecteur(float a = 0, float b = 0, float c = 0){
	x=a;
	y=b;
	z=c;
}

//L'affichage d'un vecteur
void vecteur::afficher() {
	cout << "("<<x<<", "<<y<<", "<<z<<")" << endl;
}

//Retourner la norme du vecteur
float vecteur::norme() {
	return sqrt(x*x + y*y + z*z);
}

//Le produit scalaire de deux vecteurs
float vecteur::produit(vecteur vec2) {
	return x*vec2.x + y*vec2.y + z*vec2.z;
}

//La somme de deux vecteur
vecteur vecteur::somme(vecteur vec2) {
	vecteur s;
	s.x = x + vec2.x;
	s.y = y + vec2.y;
	s.z = z + vec2.z;
	return s;
}