//
// Created by erick on 09/04/2020.
//

#include "Bibliotheque.h"

#include <iostream>
#include <algorithm>
#include <string>
#include <string.h>
#include <fstream>

void Bibliotheque::afficher() const {
    std::cout << "Affichage de la bibliotheque" << std::endl;
    for (const Livre &livre: *this) {
        std::cout << livre.getAuteur() << livre.getTitre() << livre.getAnnee() << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre() {
    sort(begin(), end());
}

void Bibliotheque::trierParAnnee() {
    sort(begin(), end(), [](Livre l1, Livre l2) -> bool {
        return l1.getAnnee() < l2.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) const {
    std::ofstream file(nomFichier);
    if (file.is_open()) {
        for (const Livre &livre : *this) {
            file << livre.getAuteur() << livre.getTitre() << livre.getAnnee() << std::endl;
        }
        file.close();
        return;
    }
    throw std::string("Le fichier ne s'est pas ouvert.");
}

void Bibliotheque::lireFichier(const std::string &nomFichier) {
    std::ifstream file(nomFichier);
    if (file.is_open()) {
        std::string delimiter = ";";
        std::string line;
        while (getline(file, line)) {
            vector<std::string> splittedString = splitString(line, delimiter);
            push_back(Livre(splittedString.at(0), splittedString.at(1), stoi(splittedString.at(2))));
        }
        file.close();
        return;
    }
    throw std::string("erreur : lecture du fichier impossible");
}

std::vector<std::string> Bibliotheque::splitString(std::string toSplit, std::string delimiter) const {
    vector<std::string> splitted;
    size_t current, previous = 0;
    current = toSplit.find(delimiter);
    while (current != std::string::npos) {
        splitted.push_back(toSplit.substr(previous, current - previous));
        previous = current + 1;
        current = toSplit.find(delimiter, previous);
    }
    //on ajoute le dernier
    splitted.push_back(toSplit.substr(previous, toSplit.length() - previous));
    return splitted;
}
