
#include <iostream>
#include "Livre.h"
#include "Bibliotheque.h"

int main() {
    Livre livre1("livre1", "moi", 2019);
    Livre livre2("livre2", "erick", 2020);
    std::cout << livre1.getTitre() << std::endl;
    std::cout << livre1.getAuteur() << std::endl;

    Bibliotheque b;
    b.push_back(livre1);
    b.push_back(livre2);

    b.afficher();

    return 0;
}

