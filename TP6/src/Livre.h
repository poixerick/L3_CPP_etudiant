//
// Created by erick on 27/03/2020.
//

#ifndef TP6_LIVRE_H
#define TP6_LIVRE_H

#include <string>
#include <iostream>

class Livre{
private:
    std::string _titre;
    std::string _auteur;
    int _annee;
public:
    Livre();
    Livre(const std::string &titre,const std::string &auteur, int annee);
    const std::string &getTitre() const;
    const std::string &getAuteur() const;
    int getAnnee() const;
};

#endif //TP6_LIVRE_H
