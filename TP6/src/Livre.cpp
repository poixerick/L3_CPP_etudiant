//
// Created by erick on 27/03/2020.
//

#include "Livre.h"

Livre::Livre(){
    _titre="titre";
    _auteur="auteur";
    _annee='2020';
}

Livre::Livre(const std::string &titre,const std::string &auteur, int annee){
    if(titre.find(";") != std::string::npos){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    else if(titre.find("\n") != std::string::npos){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }
    else{
        _titre=titre;
    }
    if(auteur.find(";") != std::string::npos){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    else if(auteur.find("\n") != std::string::npos){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }
    else{
        _auteur=auteur;
    }
    _annee=annee;
}

const std::string &Livre::getTitre() const{
    return _titre;
}

const std::string &Livre::getAuteur() const{
    return _auteur;
}

int Livre::getAnnee() const{
    return _annee;
}