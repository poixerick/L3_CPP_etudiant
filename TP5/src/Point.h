//
// Created by erick on 24/03/2020.
//

#ifndef TP4_POINT_H
#define TP4_POINT_H

struct Point{
    int _x;
    int _y;
    Point(int x, int y);
};

#endif //TP4_POINT_H
