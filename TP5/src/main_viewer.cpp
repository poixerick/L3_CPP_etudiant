//
// Created by erick on 10/04/2020.
//

#include "ViewerFigures.h"

int main(int argc, char *argv[]) {
    ViewerFigures viewerFigures(argc, argv);
    viewerFigures.run();
    return 0;
}
