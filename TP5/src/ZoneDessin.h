//
// Created by erick on 10/04/2020.
//

#ifndef TP5_ZONEDESSIN_H
#define TP5_ZONEDESSIN_H

#include <gdk/gdk.h>
#include <vector>
#include <cairomm/refptr.h>
#include <cairomm/context.h>
#include <gtkmm/drawingarea.h>
#include "FigureGeometrique.h"

class ZoneDessin : public Gtk::DrawingArea{
private:
    std::vector<FigureGeometrique *> _figures;
public:
    ZoneDessin();
    ~ZoneDessin();
protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context> &cr) override;
    bool gererClic(GdkEventButton* event);
};

#endif //TP5_ZONEDESSIN_H
