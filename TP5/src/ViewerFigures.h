//
// Created by erick on 09/04/2020.
//

#ifndef TP5_VIEWERFIGURES_H
#define TP5_VIEWERFIGURES_H

#include <gtkmm.h>
#include "ZoneDessin.h"

class ViewerFigures{
private:
    Gtk::Main _kit;
    Gtk::Window _window;
    ZoneDessin zoneDessin;
public:
    ViewerFigures(int argc,char **argv);
    void run();
};

#endif //TP5_VIEWERFIGURES_H
