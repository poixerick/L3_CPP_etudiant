//
// Created by erick on 27/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Ligne.h"

TEST_GROUP(GroupLigne) {
};

TEST(GroupLigne, get) {
    Ligne l (Couleur(1, 0, 0), Point(0, 0), Point(100, 200));
    CHECK_EQUAL(Point(0, 0)._x, l.getP0()._x);
    CHECK_EQUAL(Point(0, 0)._y, l.getP0()._y);
    CHECK_EQUAL(Point(100, 200)._x, l.getP1()._x);
    CHECK_EQUAL(Point(100, 200)._y, l.getP1()._y);
};
