//
// Created by erick on 24/03/2020.
//

#include <iostream>
#include "Ligne.h"

Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1): FigureGeometrique(couleur),_p0(p0),_p1(p1){}
/*
void Ligne::afficher() const{
    //Couleur couleur=getCouleur();
    Point p[2]={getP0(),getP1()};
    //p[1]=getP1();
    FigureGeometrique::afficher("Ligne",getCouleur(),2,p);
    //std::cout << "Ligne : " << couleur._r << "_" << couleur._g << "_" <<couleur._b << " " << p0._x << "_" << p0._y << " " << p1._x << "_" << p1._y << std::endl;
}*/

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> cr) const {
    Couleur _couleur=getCouleur();
    std::cout << "Ligne :" << _couleur._r << "_" << _couleur._g << "_" << _couleur._b << " " << _p0._x << "_" << _p0._y << " " << _p1._x << "_" << _p1._y
         << std::endl;
    cr->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
    cr->set_line_width(5.0);
    cr->move_to(_p0._x, _p0._y);
    cr->line_to(_p1._x, _p1._y);
    cr->stroke();
}


const Point &Ligne::getP0() const{
    return _p0;
}

const Point &Ligne::getP1() const{
    return _p1;
}