//
// Created by erick on 09/04/2020.
//

#include "ViewerFigures.h"
#include "ZoneDessin.h"
#include <gtkmm.h>

ViewerFigures::ViewerFigures(int argc, char **argv) : _kit(argc, argv) {
    _window.set_title("New fenetre");
    _window.set_default_size(640, 480);
    _window.add(zoneDessin);
    _window.show_all();
}

void ViewerFigures::run() {
    _kit.run(_window);
}
