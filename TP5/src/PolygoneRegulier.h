//
// Created by root on 24/03/2020.
//

#ifndef TP4_POLYGONEREGULIER_H
#define TP4_POLYGONEREGULIER_H

#include "FigureGeometrique.h"
#include <vector>

class PolygoneRegulier : public FigureGeometrique{
private:
    int _nbPoint;
    std::vector <Point> _points;
public:
    PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    //void afficher() const;
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> cr) const override;
    int getNbPoints() const;
    const Point &getPoint(int indice) const;
};

#endif //TP4_POLYGONEREGULIER_H
