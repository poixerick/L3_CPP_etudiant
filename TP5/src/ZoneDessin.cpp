//
// Created by erick on 10/04/2020.
//

#include "ZoneDessin.h"
#include "Ligne.h"
#include "PolygoneRegulier.h"

ZoneDessin::ZoneDessin(){
    _figures = {
            new Ligne(Couleur(0, 1, 0), Point(150, 480), Point(150, 200)),
            new PolygoneRegulier(Couleur(1, 0, 1), Point(150, 200), 100, 4),
            new Ligne(Couleur(0, 0, 1), Point(315, 0), Point(315, 100)),
            new PolygoneRegulier(Couleur(1, 1, 0), Point(315, 100), 50, 4),
            new Ligne(Couleur(1, 0, 0), Point(500, 480), Point(500, 200)),
            new PolygoneRegulier(Couleur(0, 1, 1), Point(500, 200), 100, 4)
    };
    add_events(Gdk::BUTTON_PRESS_MASK);
    signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));
}

ZoneDessin::~ZoneDessin(){
    for (FigureGeometrique *figure : _figures) {
        delete figure;
    }

}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
    for (FigureGeometrique *figure : _figures) {
        figure->afficher(cr);
    }
    return true;
}


bool ZoneDessin::gererClic(GdkEventButton* event){
    if (event->type == GDK_BUTTON_PRESS) {
        if (event->button == 1) {
            _figures.push_back(
                    new PolygoneRegulier(Couleur(rand() % 255 / 100.f, rand() % 256 / 100.f, rand() % 256 / 100.f),
                                         Point(event->x, event->y),
                                         rand() % 100, rand() % 10));
        }
        if (event->button == 3) {
            FigureGeometrique *figure = _figures.back();
            _figures.pop_back();
            delete figure;
        }
        get_window()->invalidate(false);
    }
    return true;

}
