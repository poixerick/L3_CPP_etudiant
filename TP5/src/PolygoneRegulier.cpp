//
// Created by root on 24/03/2020.
//

#include <math.h>
#include <iostream>
#include "PolygoneRegulier.h"

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes):FigureGeometrique(couleur),_nbPoint(nbCotes){
    for (int i = 0; i < _nbPoint; ++i) {
        float theta = i*2*M_PI/(float)_nbPoint;
        int x = centre._x + rayon * cos(theta);
        int y = centre._y + rayon * sin(theta);
        _points.push_back(Point(x,y));
    }
}
/*
void PolygoneRegulier::afficher() const {
    /*Couleur couleur=getCouleur();
    int nbPoint=getNbPoints();
    std::string val="";
    for (int i = 0; i < nbPoint; ++i) {
        Point p1 = getPoint(i);
        val = val+" "+std::to_string(p1._x)+"_"+std::to_string(p1._y);
    }
    std::cout << "PolygoneRegulier : " << couleur._r << "_" << couleur._g << "_" << couleur._b << val << std::endl;
     *
    FigureGeometrique::afficher("PolygoneRegulier",getCouleur(),getNbPoints(),_points);
}*/



void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> cr) const {
    Couleur _couleur=getCouleur();
    std::string message = std::to_string(_couleur._r) + "_" + std::to_string(_couleur._g) + "_" + std::to_string(_couleur._b);

    for (Point _point : _points) {
        message.append(" " + std::to_string(_point._x) + "_" + std::to_string(_point._y));
    }

    std::cout << "PolygoneRegulier: " << message << std::endl;

    for (int i = 0; i < _nbPoint; ++i) {
        cr->set_source_rgb(_couleur._r, _couleur._g, _couleur._b);
        cr->set_line_width(4.0);

        cr->move_to(_points[i]._x, _points[i]._y);
        if (i + 1 == _nbPoint) {
            cr->line_to(_points[0]._x, _points[0]._y);
            continue;
        }
        cr->line_to(_points[i + 1]._x, _points[i + 1]._y);
    }
    cr->stroke();
}


int PolygoneRegulier::getNbPoints() const{
    return _nbPoint;
}

const Point &PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}