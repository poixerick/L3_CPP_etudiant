#include "Inventaire.hpp"


std::ostream & operator<<(std::ostream & os, const Inventaire & i){
    for (const Bouteille& bouteille : i._bouteilles) {
        os << bouteille;
    }
    return os;
}

