#include "Controleur.hpp"

#include <iostream>
#include <fstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    chargerInventaire("dumb");
    refresh();
}

std::string Controleur::getTexte() {
    std::stringstream s;
    s << _inventaire;
    return s.str();
}

void Controleur::refresh() {
    for (auto &v : _vues) {
        v->actualiser();
    }
}

void Controleur::chargerInventaire(const std::string fileName) {
    std::ifstream myFile(fileName);
    if(myFile.is_open()) {
        std::string line;
        Bouteille b;
        while(getline(myFile, line)) {
            myFile >> b;
            _inventaire._bouteilles.push_back(b);
        }
    }
    refresh();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}


