
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>
#include <vector>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud *_ptrNoeudSuivant;
            Noeud(){
                _valeur=0;
                _ptrNoeudSuivant=nullptr;
            };
            Noeud(int valeur){
                _valeur=valeur;
                _ptrNoeudSuivant=nullptr;
            };
        };
        Noeud *_ptrTete;

    public:
        class iterator {
            private:
                Noeud *_ptrNoeudCourant;
            public:
                iterator(Noeud *ptrNoeudCourant){
                    _ptrNoeudCourant=ptrNoeudCourant;
                }

                const iterator & operator++() {
                    _ptrNoeudCourant=_ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &it) const {
                    return it._ptrNoeudCourant != _ptrNoeudCourant;
                }

                friend Liste; 
        };

    public:
        Liste(){
            this->_ptrTete = nullptr;
        }

        ~Liste(){
            clear();
        }

        void push_front(int valeur) {
            Noeud* newNoeud = new Noeud(valeur);
            newNoeud->_ptrNoeudSuivant=this->_ptrTete;
            _ptrTete=newNoeud;
        }

        int& front() const {
            return this->_ptrTete->_valeur;
        }

        void clear() {
            while(this->_ptrTete != nullptr){
                Noeud* noeud=this->_ptrTete;
                this->_ptrTete=this->_ptrTete->_ptrNoeudSuivant;
                delete(noeud);
            }
        }

        bool empty() const {
            return this->_ptrTete == nullptr;
        }

        iterator begin() const {
            return this->_ptrTete;
        }

        iterator end() const {
            return nullptr;
        }

};

std::ostream& operator<<(std::ostream& os, const Liste&liste) {
    Liste::iterator it = liste.begin();
    while(it != liste.end()) {
        os << *it << " ";
        ++it;
    }
    return os;
}

#endif

