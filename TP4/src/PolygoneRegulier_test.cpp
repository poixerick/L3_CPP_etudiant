//
// Created by erick on 27/03/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "PolygoneRegulier.h"

TEST_GROUP(GroupPR) {
};

TEST(GroupPR, getNbPoint) {
    PolygoneRegulier polygoneRegulier = PolygoneRegulier(Couleur(0, 1, 0), Point(0, 0), 1, 4);
    CHECK_EQUAL(4, polygoneRegulier.getNbPoints());
}

TEST(GroupPR, getPoint) {
    PolygoneRegulier polygoneRegulier = PolygoneRegulier(Couleur(0, 1, 0), Point(0, 0), 1, 4);
    CHECK_EQUAL(1, polygoneRegulier.getPoint(0)._x);
    CHECK_EQUAL(0, polygoneRegulier.getPoint(0)._y);
}

TEST(GroupPR, getCouleur) {
    PolygoneRegulier polygoneRegulier = PolygoneRegulier(Couleur(0, 1, 0), Point(0, 0), 1, 4);
    CHECK_EQUAL(0, polygoneRegulier.getCouleur()._r);
    CHECK_EQUAL(1, polygoneRegulier.getCouleur()._g);
    CHECK_EQUAL(0, polygoneRegulier.getCouleur()._b);
}
