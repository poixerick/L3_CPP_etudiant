//
// Created by root on 24/03/2020.
//

#ifndef TP4_POLYGONEREGULIER_H
#define TP4_POLYGONEREGULIER_H

#include "Point.h"
#include "FigureGeometrique.h"

class PolygoneRegulier : public FigureGeometrique{
private:
    int _nbPoint;
    Point *_points;
public:
    PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes);
    void afficher() const;
    const int getNbPoints() const;
    const Point &getPoint(int indice) const;
};

#endif //TP4_POLYGONEREGULIER_H
