//
// Created by erick on 24/03/2020.
//

#include <iostream>
#include "Ligne.h"

Ligne::Ligne(const Couleur &couleur, const Point &p0, const Point &p1)
    : FigureGeometrique(couleur),_p0(p0),_p1(p1){}

void Ligne::afficher() const{
    //Couleur couleur=getCouleur();
    Point p[2]={getP0(),getP1()};
    //p[1]=getP1();
    FigureGeometrique::afficher("Ligne",getCouleur(),2,p);
    //std::cout << "Ligne : " << couleur._r << "_" << couleur._g << "_" <<couleur._b << " " << p0._x << "_" << p0._y << " " << p1._x << "_" << p1._y << std::endl;
}

const Point &Ligne::getP0() const{
    return _p0;
}

const Point &Ligne::getP1() const{
    return _p1;
}