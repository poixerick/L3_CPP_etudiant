//
// Created by erick on 24/03/2020.
//

#include "Ligne.h"
#include "PolygoneRegulier.h"

#include <iostream>

int main() {
    Couleur couleur=Couleur(1,0,0);
    Point p0=Point(0,0);
    Point p1=Point(100,200);
    Ligne ligne=Ligne(couleur,p0,p1);
    ligne.afficher();
    Couleur couleurp=Couleur(0,1,0);
    Point p1p=Point(100,200);
    PolygoneRegulier pol=PolygoneRegulier(couleurp,p1p,50,5);
    pol.afficher();
    return 0;
}

