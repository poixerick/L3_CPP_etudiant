//
// Created by root on 24/03/2020.
//

#include <math.h>
#include <iostream>
#include "PolygoneRegulier.h"

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbCotes):FigureGeometrique(couleur),_nbPoint(nbCotes){
    for (int i = 0; i < _nbPoint; ++i) {
        float theta = i*2*M_PI/(float)_nbPoint;
        int x = centre._x + rayon * cos(theta);
        int y = centre._y + rayon * sin(theta);
        _points[i]=Point(x,y);
    }
}

void PolygoneRegulier::afficher() const {
    /*Couleur couleur=getCouleur();
    int nbPoint=getNbPoints();
    std::string val="";
    for (int i = 0; i < nbPoint; ++i) {
        Point p1 = getPoint(i);
        val = val+" "+std::to_string(p1._x)+"_"+std::to_string(p1._y);
    }
    std::cout << "PolygoneRegulier : " << couleur._r << "_" << couleur._g << "_" << couleur._b << val << std::endl;
     */
    FigureGeometrique::afficher("PolygoneRegulier",getCouleur(),getNbPoints(),_points);
}

const int PolygoneRegulier::getNbPoints() const{
    return _nbPoint;
}

const Point &PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}