//
// Created by erick on 24/03/2020.
//

#ifndef TP4_COULEUR_H
#define TP4_COULEUR_H

struct Couleur{
    double _r;
    double _g;
    double _b;
    Couleur(int r, int g, int b);
};

#endif //TP4_COULEUR_H
