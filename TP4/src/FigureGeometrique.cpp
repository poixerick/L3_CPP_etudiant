//
// Created by erick on 24/03/2020.
//

#include <string>
#include <iostream>
#include "Couleur.h"
#include "FigureGeometrique.h"
#include "Point.h"

FigureGeometrique::FigureGeometrique(const Couleur &couleur):_couleur(couleur){}

const Couleur &FigureGeometrique::getCouleur() const{
    return _couleur;
}

void FigureGeometrique::afficher(std::string type,const Couleur &couleur,int nbPoint, Point p[]) const {
    std::string val="";
    for (int i = 0; i < nbPoint; ++i) {
        Point p1 = p[i];
        val = val+" "+std::to_string(p1._x)+"_"+std::to_string(p1._y);
    }
    std::cout << type << " : " << couleur._r << "_" << couleur._g << "_" << couleur._b << val << std::endl;

}