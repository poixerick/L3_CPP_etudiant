//
// Created by erick on 24/03/2020.
//

#ifndef TP4_FIGUREGEOMETRIQUE_H
#define TP4_FIGUREGEOMETRIQUE_H

#include "Couleur.h"
#include "Point.h"
#include <iostream>

class FigureGeometrique{
private:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &couleur);
    const Couleur &getCouleur() const;
    void afficher(std::string type,const Couleur &couleur,int nbPoint, Point p[]) const;
};

#endif //TP4_FIGUREGEOMETRIQUE_H
