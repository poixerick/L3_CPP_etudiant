//
// Created by erick on 09/04/2020.
//

#include <iostream>
#include <fstream>
#include <cmath>
#include "Image.h"

void ecrirePnm(const Image &img, const std::string &nomFichier) {
    std::ofstream file(nomFichier);
    if (file.is_open()) {
        file << "P2 " << img.getLargeur() << " " << img.getHauteur() << " 255\n";

        for (int i = 0; i < img.getLargeur(); i++) {
            for (int j = 0; j < img.getHauteur(); j++) {
                file << img.getPixel(i, j) << " ";
            }
            file << "\n";
        }
        file.close();
        return;
    }
    throw std::string("Failed to write in file");
}

void remplir(Image &img) {
    double invL = 6 * M_PI / double(img.getLargeur());
    for (int i = 0; i < img.getHauteur(); i++) {
        for (int j = 0; j < img.getLargeur(); j++) {
            int couleur = 127.f * (1.f + cos(invL * double(j)));
            img.setPixel(i, j, couleur);
        }
    }
}

