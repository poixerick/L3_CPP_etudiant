//
// Created by erick on 09/04/2020.
//

#include "Image.h"
#include <string>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur), _pixels(new int[_largeur * _hauteur]) {
    for (int i = 0; i < _largeur * _hauteur; i++) {
        _pixels[i] = i;
    }
}

Image::~Image() {
    delete[] _pixels;
}

Image::Image(const Image &image) {
    _largeur = image._largeur;
    _hauteur = image._hauteur;
    _pixels = new int[_largeur * _hauteur];
    for (int i = 0; i < _largeur * _hauteur; i++) {
        _pixels[i] = image._pixels[i];
    }
}

int Image::getHauteur() const {
    return _hauteur;
}

int Image::getLargeur() const {
    return _largeur;
}

int Image::getPixel(int i, int j) const {
    return _pixels[_largeur * i + j];
}

void Image::setPixel(int i, int j, int couleur) {
    _pixels[_largeur * i + j] = couleur;
}

int &Image::getPixelByReference(int i, int j) {
    return _pixels[_largeur * i + j];
}

Image &Image::operator=(const Image &image) {
    _largeur = image._largeur;
    _hauteur = image._hauteur;
    if ((_largeur * _hauteur) != (image._hauteur * image._largeur)) {
        delete[] _pixels;
        _pixels = new int[_largeur * _hauteur];
    }
    for (int i = 0; i < _largeur * _hauteur; i++) {
        _pixels[i] = image._pixels[i];
    }
    return *this;
}


Image Image::bordure(const Image & img, int couleur, int epaisseur){
    Image image(img);


    //border top
    for(int i = 0; i < epaisseur;i++){
        for(int j = 0; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border bottom
    for(int i = image.getHauteur()-epaisseur; i < image.getHauteur();i++){
        for(int j = 0; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border right
    for(int i = 0; i < image.getHauteur();i++){
        for(int j = image.getLargeur()-epaisseur; j < image.getLargeur(); j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    //border left
    for(int i = 0; i < image.getHauteur();i++){
        for(int j = 0; j < epaisseur; j++){
            image._pixels[j*image.getLargeur()+i]=couleur;
        }
    }
    return image;
}