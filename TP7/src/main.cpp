//
// Created by erick on 09/04/2020.
//

#include "Image.h"
#include "WriteImage.cpp"

int main() {
    Image image(50, 50);
    std::cout << "Pixel : " << image.getPixel(3,3) << std::endl;
    std::cout << "Largeur : " << image.getLargeur() << std::endl;
    std::cout << "Hauteur : " << image.getHauteur() << std::endl;
    remplir(image);
    Image newImage = image.bordure(image, 150, 3);
    ecrirePnm(newImage, "image.pnm");

    return 0;
}
