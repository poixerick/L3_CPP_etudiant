//
// Created by erick on 09/04/2020.
//

#ifndef TP7_IMAGE_H
#define TP7_IMAGE_H

class Image {
private:
    int _largeur;
    int _hauteur;
    int *_pixels;
public:
    Image(int largeur, int hauteur);
    ~Image();
    Image(const Image& image);
    int getHauteur() const;
    int getLargeur() const;
    int getPixel(int i, int j) const;
    void setPixel(int i, int j, int couleur);
    int &getPixelByReference(int i, int j);
    Image& operator=(const Image& image);
    Image bordure(const Image & img, int couleur, int epaisseur);
};


#endif //TP7_IMAGE_H
